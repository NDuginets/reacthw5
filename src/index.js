import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';

import App from './components/App';
import { Provider } from "react-redux";
import store from './store';


import { BrowserRouter, Route, Redirect, Switch } from 'react-router-dom';

const supportsHistory = 'pushState' in window.history;

ReactDOM.render(
  <Provider store={store}>
    <BrowserRouter basename="/" forceRefrech={!supportsHistory}>
      <Switch>
        
        <Route path="/all" exact render={() => {             
            return <App status={'all'} />
        }
        } /> 
        <Route path="/succses" exact render={() => {             
            return <App status={true} />
        }
        } /> 
        <Route path="/work" exact render={() => {             
            return <App status={false} />
        }
        } /> 
        <Redirect from='/' to='/all'/>             
        <Route path="/" component={App}/>
      </Switch>  
    </BrowserRouter>
  </Provider>,
  document.getElementById('root'));
