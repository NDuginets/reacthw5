import TodoList from '../components/TodoList';
import { connect } from 'react-redux'
import ActionCreator from '../actions/demoActionCreator';

const mapStateToProps = (state, ownProps) => {
  return {
      todolist: state.todoreducer.todolist
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    addTodo: (text) => {
      dispatch( ActionCreator.addTodo(text) );
    },
    changeStatus: (index) => {
      dispatch( ActionCreator.changeStatus(index) );
    },
    delTodo: (index, array) => {
      dispatch( ActionCreator.delTodo(index, array) );
    }
  };
};

const MyList = connect(
  mapStateToProps,
  mapDispatchToProps
)(TodoList);


export default MyList;
