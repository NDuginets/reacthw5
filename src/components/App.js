import React, { Component } from 'react';
import './App.css';
import TodoList from '../containers/appContainer'


class App extends Component {
  render() {
    let {status} = this.props;
    return (
      <div className="App">
       { 
       <TodoList status={status}/>
        }
      </div>
    );
  }
}

export default App;
