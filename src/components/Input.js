import React from 'react';
import {NavLink} from 'react-router-dom';

let Input = ({addTodo}) => {
let input;
  return(

<nav class="navbar navbar-light bg-light justify-content-between">

  <NavLink   className="btn btn-info"  to={"/all"}>All</NavLink >
  <NavLink   className="btn btn-info" to={"/succses"}>Succses</NavLink >
  <NavLink   className="btn btn-info" to={"/work"}>Work</NavLink >

  <div class="form-inline">
    <input class="form-control mr-sm-2" type="search" placeholder="What wanna do" aria-label="Search" ref={node => {
      input = node;
    }}/>
    <button class="btn btn-info" type="submit" onClick={()=> {
        addTodo(input.value)
        input.value = '';
    }}>Add</button>
  </div>
</nav>
)
};

export default Input;

/* <div>
  <input ref={node => {
      input = node;
  }}/>
    <button onClick={()=> {
        addTodo(input.value)
        input.value = '';
    }}>
    Add
    </button>
  </div> */