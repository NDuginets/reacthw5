import React, { Component } from 'react';
import Todo from './Todo.js'
import Input from './Input.js'




class TodoList extends Component {


StatusFilter = (item)  => {
let {status} = this.props
  if (status === "all") {
    return true 
  } 
    else return item.completed === status
}

render () {
  let {changeStatus, addTodo, delTodo, status} = this.props;
  let filteredList = this.props.todolist.filter(this.StatusFilter);
  return(
    <div>
      <div>
        <Input addTodo={addTodo} active={status}/>
      </div>  
      <div class="row b">  
        {filteredList.map( item => <Todo key={item.id} todo={item} changeStatus={changeStatus} delTodo={delTodo} allarray={this.props.todolist}/> )}
      </div>
    </div>
  );
 }
};

export default TodoList;