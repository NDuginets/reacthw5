import React, { Component } from 'react';
import './App.css';

class Todo extends Component {

handlerClickForChange = (e) => {
    this.props.changeStatus(this.props.todo.id)
}

handlerClickForDel = (e) => {
    this.props.delTodo(this.props.todo.id, this.props.allarray)
}
    render () 
    {     
        return (
        <div  class="col-sm-3 a">
            <div  className={this.props.todo.completed === true ? 'card success':'card warning' } >
                <div class="card-body">
                    <h4 class="card-text">{this.props.todo.text}</h4>
                    <button type="button" class="btn btn-success a" onClick={this.handlerClickForChange}>Success</button>
                    <button type="button" class="btn btn-danger a" onClick={this.handlerClickForDel}>Delete</button>
                </div>
            </div>
        </div>

          
        )
    }
}

export default Todo

// <button onClick={this.props.delTodo(this.props.todo.id, this.props.allarray)}> Del</button>  удаление
// onClick={this.props.changeStatus(this.props.todo.index)}   смена статуса
//  <div className={this.props.todo.completed === true ? 'true':'false' }  onClick={this.handlerClick} >{this.props.todo.text}</div>