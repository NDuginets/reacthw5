import { combineReducers } from 'redux';


const todoInitialState = {
  todolist: [
    {
      text: 'My try',
      id: 0,
      completed: false
    },
    {
      text: 'My cry',
      id: 1,
      completed: false
    }

  ]
}

function todoreducer(state = todoInitialState, action){
  switch( action.type ){
    case "ADD":
      return Object.assign({}, state, {
        todolist: [
          ...state.todolist,
          {
            text: action.text,
            id:action.id,
            completed: false
          }
        ]
      })

   case "CHANGE_STATUS":
    return Object.assign({}, state, {
      todolist: state.todolist.map((item, index) => {
        if(item.id === action.index) {
          return Object.assign({}, item, {
            completed: !item.completed
          })
        }
        return item
      })
    })

   case "DEL":
      return Object.assign({}, state, {
        todolist: action.newArray
      })

    default:
      return state;
  }
}


const reducer = combineReducers({
  todoreducer
});

export default reducer;
