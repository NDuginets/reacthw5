let newId = 2;


const ActionCreator = {

  addTodo: (text) => {
    return function (dispatch, getState) {
      dispatch({
          type: 'ADD',
          text: text,
          id: newId++
        });
    };
  },

  changeStatus: (index) => {
    return function (dispatch, getState) {
      dispatch({
          type: 'CHANGE_STATUS',
          index: index
        });
    };
  },


  delTodo: (index, array) => {
    let newArray = [];
    array.map( (item)=> {
      if( item.id !== index){
        newArray.push(item)
      }
    })
    return function (dispatch, getState) {
      dispatch({
          type: 'DEL',
          newArray: newArray
        });
    };
  }

}









export default ActionCreator;
